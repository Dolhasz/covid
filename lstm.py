import os
import numpy as np
import tensorflow as tf
import timeseries as ts
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.utils import OrderedEnqueuer
import datetime
from dateutil import parser


def load_training_data(country='China', return_dates=False):
    df = ts.load_dataset(ts.CovidTs.confirmed)
    x, y = ts.get_ts_country(df, country)
    y = np.array(y).reshape(-1,1)
    if return_dates:
        return y, x
    else:
        return y


def training_generator(all_train, history=20):
    while True:
        r = np.random.randint(len(all_train))
        y = all_train[r]
        s = np.random.randint(len(y)-(history+1))
        e = s + history
        t = e + 1
        X = np.array(y[s:e]).reshape(-1,1).astype(np.float32)
        Y = np.array(y[t]).reshape(-1,1).astype(np.float32)
        yield np.array(X), np.array(Y)


def validation_generator(val_data, history=20):
    for s in range(len(val_data)-(history+1)):
        e = s + history
        t = e + 1
        X = np.array(val_data[s:e]).reshape(-1,1).astype(np.float32)
        Y = np.array(val_data[t]).reshape(-1,1).astype(np.float32)
        yield np.expand_dims(np.array(X), 0), np.expand_dims(np.array(Y), 0),


def build_lstm(shape=(40, 1), cells=128, output_shape=501):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.LSTM(cells, activation='relu', input_shape=shape))

    model.add(tf.keras.layers.Dropout(0.15))
    model.add(tf.keras.layers.Dense(output_shape, activation='linear'))

    optimizer = tf.keras.optimizers.Adam(lr=0.0001)
    model.compile(optimizer, loss='mse')
    print(model.summary())
    return model


def build_lstm_locs(shape=(40, 1), cells=128, output_shape=501):
    l = tf.keras.Input(shape=(2, output_shape))
    loc = tf.keras.layers.Dense(1)(l)
    loc = tf.keras.layers.Flatten()(loc)
    ts_in = tf.keras.Input(shape=shape)
    ts = tf.keras.layers.LSTM(cells, activation='relu', input_shape=shape)(ts_in)
    ts = tf.keras.layers.Dropout(0.15)(ts)
    ts = tf.keras.layers.Concatenate()([loc, ts])
    ts = tf.keras.layers.Dense(output_shape)(ts)
    model = tf.keras.Model([l, ts_in], ts)
    optimizer = tf.keras.optimizers.Adam(lr=0.001)
    model.compile(optimizer, loss='mse')
    print(model.summary())

    return model

def train_epoch(model, dataset, epoch_steps):
    epoch_loss = []
    for batch in range(epoch_steps):
        X, Y = next(dataset)
        loss = model.train_on_batch(X, Y)
        epoch_loss.append(loss)
        if batch % 10 == 1:
            print(f'Batch {batch}/{epoch_steps}')
    return np.mean(epoch_loss)


def validate_epoch(model, y_val, history, scaler):
    mses, unnorm_mses = [], []
    val_dataset = iter(validation_generator(y_val, history))
    
    for val_batch, (X, Y) in enumerate(val_dataset):
        pred = model.predict(X).reshape(-1, 1)
        unnorm_mse = np.mean(np.square(Y-pred))
        unnorm_mses.append(unnorm_mse)
        ipt = scaler.inverse_transform(X.reshape(-1,1))
        gt = scaler.inverse_transform(Y.reshape(-1,1))
        p = scaler.inverse_transform(pred)
        mses.append(np.mean(np.square(gt-p)))

    return mses, unnorm_mses


def train():
    '''Main training & validation loop'''

    # Params
    history = 20
    batch_size = 64
    epochs = 100
    epoch_steps = 100
    prefetch = 4096
    lstm_cells = 256

    # Logging
    if not os.path.exists:
        os.makedirs('logs')
    folder = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    writer = tf.summary.create_file_writer(f"./logs/{folder}/training")
    val_writer = tf.summary.create_file_writer(f"./logs/{folder}/validation")

    # Model
    model = build_lstm(shape=(history, 1), cells=lstm_cells)


    # Load training data
    all_train = []
    scaler = MinMaxScaler(feature_range=(0, 1))
    for c in ('China', 'Italy', 'Germany', 'US'):
        y = load_training_data(c)
        if c == 'Italy':
            scaler.fit(y)
            print(scaler.data_max_)
        all_train.append(y)

    # Normalize
    all_train = [scaler.transform(c) for c in all_train]
    print(np.max(all_train[1]))
    dataset = tf.data.Dataset.from_generator(
        generator=training_generator, 
        args=(all_train, history), 
        output_types=(tf.float32, tf.float32)
        ).batch(batch_size).prefetch(prefetch).shuffle(
        batch_size, seed=None, reshuffle_each_iteration=True
        )
    dataset = iter(dataset)

    # # Validation data
    val_data = load_training_data('United Kingdom')
    y_val = scaler.transform(val_data)

    # Training Loop
    min_mse = np.inf # Keep track of minimum val MSE
    for epoch in range(epochs):
        print(f'Epoch: {epoch+1}')
        
        # Train
        epoch_loss = train_epoch(model, dataset, epoch_steps)
        
        # Validate
        mses, unnorm_mses = validate_epoch(model, y_val, history, scaler)

        # Visualise
        if np.mean(mses) < min_mse:
            min_mse = np.mean(mses)
            model.save('./best_model.hdf5')

        # Log to Tensorboard
        with writer.as_default():
            tf.summary.scalar("mse_loss", np.mean(epoch_loss), step=epoch)
            writer.flush()

        with val_writer.as_default():
            tf.summary.scalar("unmse_loss", np.mean(mses), step=epoch)
            tf.summary.scalar("mse_loss", np.mean(unnorm_mses), step=epoch)
            writer.flush()
    
    return scaler

def evaluate(model_path, max_scale=47021.):
    model = tf.keras.models.load_model(model_path, compile=False)
    history = model.input_shape[1]
    y, x = load_training_data('France', return_dates=True)
    x = [parser.parse(d) for d in x]
    
    y2 = y.copy()
    y2 = [i / 1.0 for i in y2]
    y = [i / max_scale for i in y]
    # y = scaler.transform(y)

    for s in range(len(y)-history):
        print(f'{s}/{len(y)-history}')
        initial_ts = y[s:s+history]
        preds = []
        pred = model.predict(np.array(initial_ts).reshape(1,-1,1))
        preds.append(pred[0])
        for t in range(20):
            initial_ts = initial_ts[1:]
            initial_ts.append(pred[0])
            inp = np.array(initial_ts).reshape(1,-1,1)
            pred = model.predict(inp)
            preds.append(pred[0])
        plt.plot(x[s:], y2[s:], label='GT')
        plt.plot(x[s:s+history] + [x[s+history] + datetime.timedelta(days=i) for i in range(len(preds))], y2[s:s+history] + [p * max_scale for p in preds], label='Predicted')
        plt.plot(x[s:s+history], y2[s:s+history], label='Prediction input')
        plt.legend()
        plt.show()

if __name__ == "__main__":
    scaler = train()
    evaluate('best_model.hdf5')