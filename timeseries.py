import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class CovidTs:
    base_path = 'COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-'
    confirmed = f'{base_path}Confirmed.csv'
    deaths = f'{base_path}Deaths.csv'
    recovered = f'{base_path}Recovered.csv'


# def get_all_ts(df):


def get_ts_country(df, country):
    dfc = df.loc[df['Country/Region'] == country]
    if len(dfc) > 0:
        df_ts = dfc.iloc[:, 4:].agg(['sum'])
        dt = df_ts.columns.tolist()
        y = df_ts.values.tolist()[0]
        return dt, y
    else:
        raise ValueError('Unknown country')
        

def get_latlon_country(df, country):
    dfc = df.loc[df['Country/Region'] == country]
    if len(dfc) > 0:
        df_ll = dfc.loc[:, ['Lat', 'Long']].agg(['mean'])
        lat, lon = df_ll['Lat'][0], df_ll['Long'][0]
        return lat.tolist(), lon.tolist()
    else:
        raise ValueError('Unknown country')    


def plot_ts_country(df, country, title):
    plt.figure()
    if not isinstance(country, list):
        country = [country]        
    for c in country:
        x, y = get_ts_country(df, c)
        plt.plot(x, y, label=c)
        plt.title(title)
        plt.xlabel('Date')
        plt.ylabel('n cases')
        plt.xticks(x[::len(x)//8])
        plt.legend()


def load_dataset(dataset):
    return pd.read_csv(dataset)

    
if __name__ == "__main__":

    df_confirmed = load_dataset(CovidTs.confirmed)
    df_dead = load_dataset(CovidTs.deaths)
    df_recovered = load_dataset(CovidTs.recovered)

    dsets = [df_confirmed, df_dead, df_recovered]
    titles = ['Confirmed', 'Deaths', 'Recoveries']

    for d, t in zip(dsets, titles):
        plot_ts_country(d, ['Poland', 'United Kingdom', 'Germany', 'Netherlands'], t)
    plt.show()

