import os
import re
import urllib.request
import pandas as pd
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup


def download_government_data():
    if not os.path.exists('data'):
        os.mkdir('data')
    page_data = urllib.request.urlopen(
        'https://www.acaps.org/covid19-government-measures-dataset'
        ).read().decode('utf-8')
    if page_data is not None:
        soup = BeautifulSoup(page_data)
        for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):
            if link.get('href').endswith('xlsx'):
                data_url = link.get('href')
    if data_url is not None:
        dest_path = 'data/government_measures.xlsx'
        urllib.request.urlretrieve(data_url, dest_path)
        print(f'Updated Government Measures Data to: {dest_path}')
        return dest_path


def load_government_data(path):
    return pd.read_excel(path, sheet_name=2)


def test():
    path = 'data/government_measures.xlsx'
    if not os.path.exists(path):
        path = download_government_data()
    df = load_government_data(path)

    countries_measures = df.groupby('COUNTRY')['MEASURE'].count().sort_values()

    plt.figure(figsize=(15,5))
    plt.title('Number of reported government measures per country')
    plt.bar(countries_measures.index, countries_measures.tolist())
    plt.ylabel('N Measures')
    plt.xticks(rotation=90)
    plt.tick_params(axis='x', which='major', pad=5)
    plt.gcf().subplots_adjust(bottom=0.5)
    plt.show()


if __name__ == "__main__":
    test()