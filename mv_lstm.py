# %% 
import os
import datetime
import timeseries as ts 
import tensorflow as tf
import lstm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [10, 5]


def window(cases, start, duration):
    return (cases[:, start:start+duration], 
        cases[:, start+duration+1])


def get_daily_cases_timeseries(df, drop=None, select=None):
    if drop is not None:
        df = df[df['Country/Region'] != drop]
    if select is not None:
        df = df[df['Country/Region'] == select]
    cases = df.iloc[:,4:].diff(axis=1).fillna(0)
    cases.columns = pd.to_datetime(cases.columns)
    locations = df.iloc[:, 2:4].transpose()
    return cases, locations

    
def sample_range(df, history):
    assert (history + 2 < df.shape[1], 
        f'History parameter must be lower than {df.shape[1]-2}')
    start = np.random.randint(df.shape[1]-history-2)
    w, t = window(df, start, history)
    return w.transpose(), t


def ts_generator(df, history, batch_size, n_countries, locations):
    while True:
        X, Y, L = [], [], []
        for b in range(batch_size):
            x, y = sample_range(df, history)
            X.append(x.reshape(history, n_countries).astype(np.float32))
            Y.append(y.astype(np.float32))
            L.append(locations)

        yield {'input_1': np.array(L).reshape(-1, 2, n_countries), 
            'input_2' : np.array(X).reshape(-1, history, n_countries)
        }, np.array(Y).reshape(-1,n_countries)


def plot_sample(x, y):
    plt.plot(x.index, x)
    plt.plot(np.repeat(y.name, len(y)), y.transpose(), 'x r')
    plt.show()


def load_lstm_data(dataset):
    df = ts.load_dataset(dataset)
    cases, locations = get_daily_cases_timeseries(df)
    df_max = cases.max().max()
    norm_cases = cases / df_max
    print(
        f'''***=== Scaling by dataset-wide maximum: {df_max} ===***'''
    )

    loc_max = locations.max().max()
    locations = locations / loc_max
    return norm_cases, cases, df_max, locations, loc_max
    

def train(dataset=ts.CovidTs.confirmed, history=20, batch_size=32):
    # Prepare data
    norm_cases, cases, df_max, locs, loc_max = load_lstm_data(dataset)
    n_countries = len(norm_cases)
    train_cases = norm_cases.iloc[:, history+1:]
    val_cases = norm_cases.iloc[:, :history+1].to_numpy()

    dataset = tf.data.Dataset.from_generator(
        generator=ts_generator,
        args=(train_cases.to_numpy(), history, batch_size, n_countries, locs.to_numpy()),
        output_types=({'input_1':tf.float32, 'input_2':tf.float32}, tf.float32),
        output_shapes=(
            {'input_1':tf.TensorShape((batch_size, 2, n_countries)), 'input_2':tf.TensorShape((batch_size, history, n_countries))}, 
            tf.TensorShape((batch_size, n_countries)))
    )

    # Logging
    if not os.path.exists:
        os.makedirs('logs')
    folder = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    
    # Build & train model
    model = lstm.build_lstm_locs(
        shape=(history, n_countries), 
        cells=128, 
        output_shape=n_countries
    )
    model.fit(
        dataset, 
        steps_per_epoch=100, 
        epochs=1000, 
        validation_data=(
            [locs.to_numpy().reshape(-1, 2, n_countries), val_cases[:,:val_cases.shape[1]-1].reshape(-1, history, n_countries)], 
            val_cases[:,-1].reshape(-1, n_countries),
        ),
        validation_steps=1,
        verbose=2,
        callbacks=[
            tf.keras.callbacks.TensorBoard(log_dir=f'logs\\{folder}'),
            tf.keras.callbacks.ModelCheckpoint('./best_mv_model.hdf5', 
                monitor='val_loss', save_best_only=True),
            tf.keras.callbacks.EarlyStopping(patience=300)
        ]
    )

def evaluate(dataset, history, s=None):
    # Load last best model
    model = tf.keras.models.load_model('./best_mv_model.hdf5')
    # Load dataset of cases
    norm_cases, cases, df_max, locs, loc_max = load_lstm_data(dataset)
    print(len(norm_cases.columns))
    n_countries = len(norm_cases)
    
    # Generate random input data
    if s is None:
        s = np.random.randint(len(norm_cases.columns)-history-1)
    e = s + history
    t = e + 1

    X = norm_cases.iloc[:, s:e].transpose().to_numpy()
    Y = norm_cases.iloc[:, t].to_numpy()
    L = norm_cases.columns.tolist()[s:e+1]

    preds = model.predict([np.expand_dims(locs.to_numpy(), 0), np.expand_dims(X, 0)])
    gt = np.concatenate([X, Y.reshape(1,-1)], axis=0)* df_max

    plt.plot(L, gt)
    plt.plot(L[-1], (Y.reshape(1,-1)* df_max), 'o-g')
    plt.plot(L[-1], (preds* df_max), 'x-r')
    print(f'Mean Absolute error (training set): {np.mean(np.abs(Y * df_max - preds * df_max))}')
    plt.show()

    # preds = model.predict(np.concatenate([X[1:], preds]).reshape(1,history, -1))


def get_country_idx(dataset, query):
    return dataset.index[dataset['Country/Region'] == query] 

if __name__ == "__main__":
    history = 4
    batch_size = 32
    dataset = ts.CovidTs.confirmed


    train(dataset, history, batch_size)

    df = ts.load_dataset(dataset)
    load_lstm_data(dataset)

    for s in range(55):
        evaluate(dataset, history, s=s)
    


# %%


# %%
