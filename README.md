# Create conda env and install dependancies

```
conda env create -f conda_env.yml
```

```
pip install -r requirements.txt
```

# Test data loaders

```
python timeseries.py
```

```
python government_measures.py
```


# Train and evaluate dummy LSTM on data

```
python train.py
```

See code for more details.
